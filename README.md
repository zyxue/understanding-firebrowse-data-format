
This repo attempts to understand the file formats from Firehose/Firebrowse. I
will explore more types of data (currently limited to RNA-Seq) as I encounter
them.

A more detailed post can be found here:
http://zyxue.github.io/2017/06/02/understanding-TCGA-mRNA-Level3-analysis-results-files-from-firebrose.html


I will use KICH as the example as it is of the smalled data size with relative
few normal and tumour samples.

All files are downloaded from http://firebrowse.org/ as tar ball, and then
extracted.
